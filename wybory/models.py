from django.db import models

class Okreg(models.Model):
    wojewodztwo = models.CharField(max_length=100)

    def __str__(self):
        return str(self.id) + " " + self.wojewodztwo

class Gmina(models.Model):
    id = models.CharField(max_length=100, primary_key=True)
    powiat = models.CharField(max_length=100)
    nazwa_gminy = models.CharField(max_length=100)
    okreg = models.ForeignKey(Okreg, on_delete=models.CASCADE)

    def __str__(self):
        return self.id + " : " + self.nazwa_gminy

class Obwod(models.Model):

    obwod = models.IntegerField()
    glosy_niewazne = models.IntegerField()
    glosy_oddane = models.IntegerField()
    glosy_wazne = models.IntegerField()
    karty_wydane = models.IntegerField()
    uprawnieni = models.IntegerField()
    gmina = models.ForeignKey(Gmina, on_delete=models.CASCADE)
    adres = models.CharField(max_length=100)

    def __str__(self):
        return str(self.obwod) + " " + self.adres

class Kandydat(models.Model):
    nazwa = models.CharField(max_length=100)
    def __str__(self):
        return str(self.nazwa)


class Wynik(models.Model):
    obwod = models.ForeignKey(Obwod, on_delete=models.CASCADE)
    kandydat = models.ForeignKey(Kandydat, on_delete=models.CASCADE)
    glosy = models.IntegerField()
    def __str__(self):
        return self.kandydat.nazwa + " wynik: " + str(self.glosy)