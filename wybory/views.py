
from django.http import Http404
from django.http import HttpResponseRedirect
from django.db.models import Sum, Count

from django.shortcuts import render

from .forms import SearchForm
from .forms import KandydatEditForm
from . import models

nazwy_wojewodztw = {
    'DOLNOŚLĄSKIE': 1,
    'KUJAWSKO-POMORSKIE': 2,
    'ŁÓDZKIE': 3,
    'LUBELSKIE': 4,
    'LUBUSKIE': 5,
    'MAZOWIECKIE':6,
    'MAŁOPOLSKIE': 7,
    'OPOLSKIE': 8,
    'PODKARPACKIE': 9,
    'PODLASKIE': 10,
    'POMORSKIE': 11,
    'ŚLĄSKIE': 12,
    'ŚWIĘTOKRZYSKIE': 13,
    'WARMIŃSKO-MAZURSKIE': 14,
    'WIELKOPOLSKIE': 15,
    'ZACHODNIOPOMORSKIE': 16
}


def wyliczProcent(wyniki, wszystkie_glosy):

    for wynik in wyniki:
        wynik['procent'] = round(wynik['glosy'] / wszystkie_glosy * 100, 5)

    return wyniki


def index(request):
    wyniki_kandydatow = models.Wynik.objects.values('kandydat__nazwa').annotate(glosy=Sum('glosy'))
    wyniki_obwodow = models.Obwod.objects.aggregate(glosy_wazne=Sum('glosy_wazne'), karty_wydane=Sum('karty_wydane'), uprawnieni=Sum('uprawnieni'),
                    glosy_oddane=Sum('glosy_oddane'), glosy_niewazne=Sum('glosy_niewazne'), obwodow=Count('id'))


    wyniki_kandydatow = wyliczProcent(wyniki_kandydatow, wyniki_obwodow['glosy_wazne'])

    context = {
        'wojewodztwa': nazwy_wojewodztw,
        'wyniki_kandydatow': wyniki_kandydatow,
        'wyniki_obwodow': wyniki_obwodow,
        'form': SearchForm()
    }
    return render(request, 'wybory/index.html', context)

def wojewodztwo(request, wojewodztwo_id):
    wyniki_kandydatow = models.Wynik.objects.filter(obwod__gmina__okreg__wojewodztwo=wojewodztwo_id)\
                            .values('kandydat__nazwa').annotate(glosy=Sum('glosy'))

    if wyniki_kandydatow.first() is None:
        raise Http404("Nie ma takiego wojewodztwa")

    wyniki_obwodow = models.Obwod.objects.filter(gmina__okreg__wojewodztwo=wojewodztwo_id)\
                            .aggregate(glosy_wazne=Sum('glosy_wazne'), karty_wydane=Sum('karty_wydane'), uprawnieni=Sum('uprawnieni'),
                                glosy_oddane=Sum('glosy_oddane'), glosy_niewazne=Sum('glosy_niewazne'), obwodow=Count('id'))

    lista_obiektow = models.Wynik.objects.filter(obwod__gmina__okreg__wojewodztwo=wojewodztwo_id).values('obwod__gmina__okreg__id').distinct()

    wyniki_kandydatow = wyliczProcent(wyniki_kandydatow, wyniki_obwodow['glosy_wazne'])

    context = {
        'wojewodztwa': nazwy_wojewodztw,
        'wyniki_kandydatow': wyniki_kandydatow,
        'wyniki_obwodow': wyniki_obwodow,
        'lista_obiektow': lista_obiektow,
        'form': SearchForm(),
        'opis': wojewodztwo_id
    }

    return render(request, 'wybory/wojewodztwo.html', context)

def okreg(request, okreg_id):
    wyniki_kandydatow = models.Wynik.objects.filter(obwod__gmina__okreg__id=okreg_id)\
                            .values('kandydat__nazwa').annotate(glosy=Sum('glosy'))

    if wyniki_kandydatow.first() is None:
        raise Http404("Nie ma takiego okregu")

    wyniki_obwodow = models.Obwod.objects.filter(gmina__okreg__id=okreg_id)\
                            .aggregate(glosy_wazne=Sum('glosy_wazne'), karty_wydane=Sum('karty_wydane'), uprawnieni=Sum('uprawnieni'),
                                glosy_oddane=Sum('glosy_oddane'), glosy_niewazne=Sum('glosy_niewazne'), obwodow=Count('id'))

    lista_obiektow = models.Wynik.objects.filter(obwod__gmina__okreg__id=okreg_id).values('obwod__gmina__id', 'obwod__gmina__nazwa_gminy').distinct()
    wyniki_kandydatow = wyliczProcent(wyniki_kandydatow, wyniki_obwodow['glosy_wazne'])

    context = {
        'wojewodztwa': nazwy_wojewodztw,
        'wyniki_kandydatow': wyniki_kandydatow,
        'wyniki_obwodow': wyniki_obwodow,
        'lista_obiektow': lista_obiektow,
        'form': SearchForm(),
        'opis': okreg_id
    }

    return render(request, 'wybory/okreg.html', context)

def gmina(request, gmina_id):
    wyniki_kandydatow = models.Wynik.objects.filter(obwod__gmina__id=gmina_id)\
                            .values('kandydat__nazwa').annotate(glosy=Sum('glosy'))
    if wyniki_kandydatow.first() is None:
        raise Http404("Nie ma takiej gminy")


    wyniki_obwodow = models.Obwod.objects.filter(gmina__id=gmina_id)\
                            .aggregate(glosy_wazne=Sum('glosy_wazne'), karty_wydane=Sum('karty_wydane'), uprawnieni=Sum('uprawnieni'),
                                glosy_oddane=Sum('glosy_oddane'), glosy_niewazne=Sum('glosy_niewazne'), obwodow=Count('id'))

    lista_obiektow = models.Wynik.objects.filter(obwod__gmina__id=gmina_id).values('obwod__id', 'obwod__adres').distinct()
    wyniki_kandydatow = wyliczProcent(wyniki_kandydatow, wyniki_obwodow['glosy_wazne'])

    gmina = models.Gmina.objects.get(id=gmina_id)

    context = {
        'wojewodztwa': nazwy_wojewodztw,
        'wyniki_kandydatow': wyniki_kandydatow,
        'wyniki_obwodow': wyniki_obwodow,
        'lista_obiektow': lista_obiektow,
        'form': SearchForm(),
        'opis': gmina.nazwa_gminy
    }

    return render(request, 'wybory/gmina.html', context)

def obwod(request, obwod_id):
    wyniki_kandydatow = models.Wynik.objects.filter(obwod__id=obwod_id) \
        .values('kandydat__nazwa', 'kandydat__id').annotate(glosy=Sum('glosy'))

    if wyniki_kandydatow.first() is None:
        raise Http404("Nie ma takiego obwodu")


    wyniki_obwodow = models.Obwod.objects.filter(id=obwod_id) \
        .aggregate(glosy_wazne=Sum('glosy_wazne'), karty_wydane=Sum('karty_wydane'), uprawnieni=Sum('uprawnieni'),
                   glosy_oddane=Sum('glosy_oddane'), glosy_niewazne=Sum('glosy_niewazne'), obwodow=Count('id'))


    wyniki_kandydatow = wyliczProcent(wyniki_kandydatow, wyniki_obwodow['glosy_wazne'])

    context = {
        'id': obwod_id,
        'wojewodztwa': nazwy_wojewodztw,
        'wyniki_kandydatow': wyniki_kandydatow,
        'wyniki_obwodow': wyniki_obwodow,
        'form': SearchForm(),
        'opis': obwod_id
    }

    return render(request, 'wybory/obwod.html', context)

def edit_kandydat(request, obwod_id, kandydat_id):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/wybory/')

    if request.method == 'POST':
        form = KandydatEditForm(request.POST)

        if form.is_valid():
            wynik = models.Wynik.objects.filter(obwod__id=obwod_id, kandydat__id=kandydat_id).first()

            if wynik is None:
                raise Http404("Nie ma takiego rekordu")

            nowe_glosy = form.cleaned_data['ilosc_glosow']
            diff = wynik.glosy - nowe_glosy
            obwod = models.Obwod.objects.filter(id=obwod_id).first()

            obwod.glosy_oddane -= diff
            obwod.glosy_wazne -= diff
            obwod.glosy_niewazne = obwod.glosy_oddane - obwod.glosy_wazne
            obwod.save()

            wynik.glosy = form.cleaned_data['ilosc_glosow']
            wynik.save()

            return HttpResponseRedirect('/wybory/obwod/' + str(obwod_id))

    else:
        wynik = models.Wynik.objects.filter(obwod__id=obwod_id, kandydat__id=kandydat_id).first()
        if wynik is None:
            raise Http404("Nie ma takiego rekordu")

        form = KandydatEditForm(initial={'ilosc_glosow': wynik.glosy})

    context = {
        'form': form,
        'obwod_id': obwod_id,
        'wynik': wynik
    }
    return render(request, 'wybory/edit_kandydat.html', context)

def gmina_search(request):
    if request.method == 'POST':
        form = SearchForm(request.POST)

        if form.is_valid():
            if len(form.cleaned_data['q']) < 3:
                error_message = "*minimum 3 znaki"
                context = {
                    'error_message': error_message,
                    'form': SearchForm()
                }
                return render(request, 'wybory/gmina_search.html', context)

            lista_obiektow = models.Gmina.objects.filter(nazwa_gminy__icontains=form.cleaned_data['q'])
            context = {
                'lista_obiektow': lista_obiektow,
                'form': form
            }
            return render(request, 'wybory/gmina_search.html', context)


    form = SearchForm()

    context = {
        'form': form
    }

    return render(request, 'wybory/gmina_search.html', context)

