from django.contrib import admin
from .models import Gmina
from .models import Kandydat
from .models import Obwod
from .models import Okreg
from .models import Wynik

# Register your models here.

admin.site.register(Gmina)
admin.site.register(Kandydat)
admin.site.register(Obwod)
admin.site.register(Okreg)
admin.site.register(Wynik)
