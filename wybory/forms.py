from django import forms

class NameForm(forms.Form):
    your_name = forms.CharField(label='Your name', max_length=100)

class KandydatEditForm(forms.Form):
    ilosc_glosow = forms.IntegerField(label='Ilość głosów')

class SearchForm(forms.Form):
    q = forms.CharField(label='Wyszukiwarka gmin', max_length=100, min_length=3)