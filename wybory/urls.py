from django.conf.urls import url
from . import views

urlpatterns = [
    # /wybory/
    url(r'^$', views.index, name='index'),

    #/wybory/wojewodztwo/id
    url(r'^wojewodztwo/(?P<wojewodztwo_id>[\w\-]+)/$', views.wojewodztwo, name='wojewodztwo'),

    #/wybory/gmina/id/
    url(r'^gmina/(?P<gmina_id>[0-9]+)/$', views.gmina, name='gmina'),

    #edit obwod
    url(r'^obwod/(?P<obwod_id>[0-9]+)/edit/kandydat/(?P<kandydat_id>[0-9]+)$', views.edit_kandydat, name='edit_obwod'),

    #/wybory/okreg/id/
    url(r'^okreg/(?P<okreg_id>[0-9]+)/$', views.okreg, name='okreg'),

    #/wybory/obwod/id/
    url(r'^obwod/(?P<obwod_id>[0-9]+)/$', views.obwod, name='obwod'),

    url(r'^search/', views.gmina_search, name='search_gmina')
]
